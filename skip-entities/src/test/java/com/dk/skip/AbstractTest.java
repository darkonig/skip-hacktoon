package com.dk.skip;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

/**
 * @author Caio Andrade (dk)
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationConfiguration.class)
@Sql({"/insert-data.sql"})
@Sql(scripts = "/clean-up.sql", executionPhase = AFTER_TEST_METHOD)
@ActiveProfiles("test")
public abstract class AbstractTest {

}