package com.dk.skip.exceptions;

/**
 * Exception para entidade já registrada
 *
 * @author Caio Andrade (dk)
 */
public class EntityRegisteredException extends RuntimeException {

    public EntityRegisteredException() {
        super();
    }

    public EntityRegisteredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EntityRegisteredException(final String message) {
        super(message);
    }

    public EntityRegisteredException(final Throwable cause) {
        super(cause);
    }
}

