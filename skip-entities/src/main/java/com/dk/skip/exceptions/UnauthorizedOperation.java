package com.dk.skip.exceptions;

/**
 * @author Caio Andrade (dk)
 */
public class UnauthorizedOperation extends RuntimeException {

    public UnauthorizedOperation() {
        super();
    }

    public UnauthorizedOperation(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedOperation(final String message) {
        super(message);
    }

    public UnauthorizedOperation(final Throwable cause) {
        super(cause);
    }

}
