package com.dk.skip.core;

import com.dk.skip.data.model.Location;

/**
 * @author Caio Andrade (dk)
 */
public class LocationUtil {

    /**
     * Calculates the end-point from a given source at a given range (meters)
     * and bearing (degrees). This methods uses simple geometry equations to
     * calculate the end-point.
     *
     * @param point
     *           Point of origin
     * @param range
     *           Range in meters
     * @param bearing
     *           Bearing in degrees
     * @return End-point from the source given the desired range and bearing.
     */
    public static Location calculateDerivedPosition(Location point,
                                                    double range, double bearing) {
        double earthRadius = 6371000; // meters

        double latA = Math.toRadians(point.getLatitude());
        double lonA = Math.toRadians(point.getLongitude());
        double angularDistance = range / earthRadius;
        double trueCourse = Math.toRadians(bearing);

        double lat = Math.asin(
                Math.sin(latA) * Math.cos(angularDistance) +
                        Math.cos(latA) * Math.sin(angularDistance)
                                * Math.cos(trueCourse));

        double dlon = Math.atan2(
                Math.sin(trueCourse) * Math.sin(angularDistance)
                        * Math.cos(latA),
                Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat));

        double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

        lat = Math.toDegrees(lat);
        lon = Math.toDegrees(lon);

        return Location.of(lat, lon);
    }

}
