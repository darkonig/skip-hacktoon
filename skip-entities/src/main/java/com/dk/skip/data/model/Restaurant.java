package com.dk.skip.data.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Caio Andrade (dk)
 */
@Entity
@Table(name = "restaurants", schema = "skip")
public class Restaurant extends BaseModel{

    @NotNull
    @Size(min = 2, max = 30)
    @Column(nullable = false, length = 30)
    private String name;

    @NotNull
    @Embedded
    private Location location;

    @ManyToOne
    private RestaurantCategoryCountry cuisine;

    private Category category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public RestaurantCategoryCountry getCuisine() {
        return cuisine;
    }

    public void setCuisine(RestaurantCategoryCountry cuisine) {
        this.cuisine = cuisine;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
