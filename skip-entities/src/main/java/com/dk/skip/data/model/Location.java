package com.dk.skip.data.model;

import java.io.Serializable;

/**
 * @author Caio Andrade (dk)
 */
public class Location implements Serializable {
    private Double longitude;
    private Double latitude;

    public Location() {
        super();
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public static Location of(Double lat, Double lon) {
        Location l = new Location();
        l.setLatitude(lat);
        l.setLongitude(lon);
        return l;
    }
}
