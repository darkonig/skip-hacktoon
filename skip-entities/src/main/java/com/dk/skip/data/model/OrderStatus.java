package com.dk.skip.data.model;

/**
 * @author Caio Andrade (dk)
 */
public enum OrderStatus {
    ACCEPTED, DELIVERED, CANCELED;
}
