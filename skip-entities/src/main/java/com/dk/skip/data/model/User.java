package com.dk.skip.data.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
@Entity
@Table(name = "users", schema = "skip")
public class User extends BaseModel {
    @NotNull
    @Size(min = 2, max = 30)
    @Column(name = "first_name", nullable = false, length = 30)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 30)
    @Column(name = "last_name", nullable = false, length = 30)
    private String lastName;

    @NotNull
    @Size(min = 10, max = 100)
    @Column(unique=true, nullable = false, length = 100)
    private String email;

    @NotNull
    @Column(nullable = false, length = 60)
    private String password;

    @NotNull
    private boolean active;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Role> roles;

    public User() {}

    public User(User user) {
        this.id = user.id;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.email = user.email;
        this.password = user.password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", active=" + active +
                ", roles=" + roles +
                '}';
    }
}

