package com.dk.skip.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Caio Andrade (dk)
 */
@Entity
@Table(name="restaurant_category_countries", schema = "skip")
public class RestaurantCategoryCountry implements Serializable {
    @Id
    @Size(min=5, max=50)
    @NotEmpty
    @Column(nullable = false, length = 50)
    private String name;

    public RestaurantCategoryCountry() {
        super();
    }

    public RestaurantCategoryCountry(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RestaurantCategoryCountry)) return false;
        RestaurantCategoryCountry that = (RestaurantCategoryCountry) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
