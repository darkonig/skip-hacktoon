package com.dk.skip.data.model;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Caio Andrade (dk)
 */
@Entity
@Table(name = "orders", schema = "skip")
public class Order extends BaseModel {

    @ManyToOne
    private Restaurant restaurant;

    @ManyToOne
    private User user;

    @Column
    private LocalDateTime scheduled;

    @Column(nullable = false, length = 8)
    private float value;

    @Column(nullable = false, length = 6)
    private float deliveryFee;

    @Column
    private OrderStatus status;

    // preview date
    @Column
    private LocalDateTime expectedCollection;

    // TODO add ordered items


    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getScheduled() {
        return scheduled;
    }

    public void setScheduled(LocalDateTime scheduled) {
        this.scheduled = scheduled;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public LocalDateTime getExpectedCollection() {
        return expectedCollection;
    }

    public void setExpectedCollection(LocalDateTime expectedCollection) {
        this.expectedCollection = expectedCollection;
    }
}
