package com.dk.skip.data.model;

import javax.persistence.*;

@Entity
@Table(name="answers", schema = "skip")
public class Answer extends BaseModel {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question", nullable = false)
    private Question question;

    @Column(length=100, nullable = false)
    private String answer;

    @Column(length=50, nullable = false)
    private String value;

    public Answer() {
    }

    public Answer(Question question, String answer, String value) {
        this.question = question;
        this.answer = answer;
        this.value = value;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
