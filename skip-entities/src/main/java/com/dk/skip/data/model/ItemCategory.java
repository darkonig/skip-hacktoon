package com.dk.skip.data.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item_categories", schema = "skip")
public class ItemCategory extends BaseModel {

    @OneToOne
    private Item item;

    @OneToOne
    private Category category;

    public ItemCategory() {
    }

    public ItemCategory(Item item, Category category) {
        this.item = item;
        this.category = category;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
