package com.dk.skip.data.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
@Entity
@Table(name = "items", schema = "skip")
public class Item extends BaseModel {

    @NotNull
    @ManyToOne
    private Restaurant restaurant;

    @NotNull
    @Size(min = 2, max = 30)
    @Column(nullable = false, length = 30)
    private String name;

    @NotNull
    @Digits(integer = 6, fraction = 2)
    @Column(nullable = false, length = 8)
    private float value;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ItemCategory> categories;

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public List<ItemCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<ItemCategory> categories) {
        this.categories = categories;
    }
}
