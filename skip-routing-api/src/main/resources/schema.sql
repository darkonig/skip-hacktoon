create schema if not exists skip;
/*
drop table skip.items if exists;
drop table skip.orders if exists;
drop table skip.restaurant_category_countries if exists;
drop table skip.restaurants if exists;
drop table skip.roles if exists;
drop table skip.users if exists;
drop table skip.users_roles if exists;
drop table skip.questions if exists;

create table skip.items (id varchar(32) not null, name varchar(30) not null, value float not null, restaurant_id varchar(32) not null, primary key (id));
create table skip.orders (id varchar(32) not null, delivery_fee float not null, expected_collection timestamp, scheduled timestamp, status integer, value float not null, restaurant_id varchar(32), user_id varchar(32), primary key (id));
create table skip.restaurant_category_countries (name varchar(50) not null, primary key (name));
create table skip.restaurants (id varchar(32) not null, latitude double, longitude double, name varchar(30) not null, category_name varchar(50), primary key (id));
create table skip.roles (role varchar(50) not null, primary key (role));
create table skip.users (id varchar(32) not null, active boolean not null, email varchar(100) not null, first_name varchar(30) not null, last_name varchar(30) not null, password varchar(60) not null, primary key (id));
create table skip.users_roles (user_id varchar(32) not null, roles_role varchar(50) not null);
alter table skip.users add constraint UK_6dotkott2kjsp8vw4d0m25fb7 unique (email);
alter table skip.users_roles add constraint UK_bp9jqv29t5ahugqsoedr7dgak unique (roles_role);
alter table skip.items add constraint FK8a8t8b4mnfx3lpdc3gf5e2bnb foreign key (restaurant_id) references restaurants;
alter table skip.orders add constraint FK2m9qulf12xm537bku3jnrrbup foreign key (restaurant_id) references restaurants;
alter table skip.orders add constraint FK32ql8ubntj5uh44ph9659tiih foreign key (user_id) references users;
alter table skip.restaurants add constraint FKgw983j376gxuctnrlbbkyggb9 foreign key (category_name) references restaurant_category_countries;
alter table skip.users_roles add constraint FK3k34bms5ip9m02vrm2ws0srlf foreign key (roles_role) references roles;
alter table skip.users_roles add constraint FK2o0jvgh89lemvvo17cbqvdxaa foreign key (user_id) references users;*/
