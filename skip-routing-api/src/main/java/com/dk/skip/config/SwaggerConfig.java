package com.dk.skip.config;

import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
//import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Configuração do Swagger
 *
 * @author Caio Andrade (dk)
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        /*return new Docket(DocumentationType.SWAGGER_2)
                .groupName("financial-api")
                .apiInfo(apiInfo())
                .ignoredParameterTypes(AuthenticationPrincipal.class)
                .select()
//                    .apis(RequestHandlerSelectors.any())
//                    .paths(PathSelectors.any())
                .paths(regex("/api.*"))
                .build();*/

        /*List<SecurityScheme> schemeList = new ArrayList<>();
        schemeList.add(new ApiKey(HttpHeaders.AUTHORIZATION, SecurityConstants.TOKEN_PREFIX, "header"));
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("financial-api")
                .apiInfo(apiInfo())
                .produces(Collections.singleton("application/json"))
                .consumes(Collections.singleton("application/json"))
                .ignoredParameterTypes(Authentication.class)
                .securitySchemes(schemeList)
                .useDefaultResponseMessages(false)
                .ignoredParameterTypes(AuthenticationPrincipal.class)
                .select()
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .paths(PathSelectors.any())
                .build();*/

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("financial-api")
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .ignoredParameterTypes(Principal.class)
                .select()
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .paths(PathSelectors.any())
                .build()
                //.securitySchemes(Lists.newArrayList(apiKey()))
                .apiInfo(apiInfo());
    }

    /*@Bean
    SecurityScheme apiKey() {
        return new ApiKey(SecurityConstants.TOKEN_PREFIX, SecurityConstants.HEADER_STRING, "header");
    }*/

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Financial API")
                .description("Financial Manager API")
                .version("1.0")
                .build();
    }
}