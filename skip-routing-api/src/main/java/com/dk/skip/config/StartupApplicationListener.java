package com.dk.skip.config;


import com.dk.skip.data.model.*;
import com.dk.skip.repository.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.fabric8.utils.PasswordEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Inicializa a base de dados com os dados de data.json
 *
 * @author Caio Andrade (dk)
 */
@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupApplicationListener.class);

    @Inject
    private UserRepository repository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private ObjectMapper mapper;

    @Value("classpath:data.json")
    private Resource res;

    @Inject
    private RestaurantRepository restaurantRepository;

    @Inject
    private RestaurantCategoryCountryRepository restaurantCategoryCountryRepo;

    @Inject
    private RestaurantCategoryRepository restaurantCategoryRepository;

    @Inject
    private QuestionRepository questionRepository;

    @Inject
    private AnswerRepository answerRepository;

    @Inject
    private ItemRepository itemRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        LOGGER.debug("Applicaton started");
        PageRequest pageRequest = PageRequest.of(0, 1);
        Page<User> items = repository.findAll(pageRequest);

        if (items.getTotalElements() == 0) {
            LOGGER.debug("Initializing DB");
            try {
                Path path = Paths.get(res.getURI());
                String s = new String(Files.readAllBytes(path));

                List<User> users = mapper.readValue(s, new TypeReference<List<User>>() { });

                users.forEach(e -> e.setPassword(PasswordEncoder.encode(e.getPassword())) );

                List<Role> roles = users.stream().flatMap(u -> u.getRoles().stream()).collect(Collectors.toList());
                roleRepository.saveAll(roles);

                repository.saveAll(users);
            } catch (IOException e) {
                e.printStackTrace();
            }


            Category categoryFit = new Category("Fit");
            Category categoryVeg = new Category("Veg");
            Category categorySteakhouse = new Category("Steakhouse");

            restaurantCategoryRepository.saveAll(Arrays.asList(categoryFit, categoryVeg, categorySteakhouse));

            RestaurantCategoryCountry countryGermany = new RestaurantCategoryCountry("Germany");
            RestaurantCategoryCountry countryIceland = new RestaurantCategoryCountry("Iceland");
            RestaurantCategoryCountry countryMexico = new RestaurantCategoryCountry("Mexico");
            RestaurantCategoryCountry countrySweden = new RestaurantCategoryCountry("Sweden");

            restaurantCategoryCountryRepo.saveAll(Arrays.asList(countryGermany, countryIceland, countryMexico, countrySweden));

            Restaurant r1 = new Restaurant();
            r1.setLocation(Location.of(-23.559361, -46.634948));
            r1.setName("Jardins");
            r1.setCuisine(countryGermany);
            r1.setCategory(categoryVeg);

            Restaurant r2 = new Restaurant();
            r2.setLocation(Location.of(-23.559808, -46.635672));
            r2.setName("Jardins Iceland");
            r2.setCuisine(countryIceland);
            r2.setCategory(categoryFit);

            Restaurant r3 = new Restaurant();
            r3.setLocation(Location.of(-23.557774, -46.635744));
            r3.setName("Jardins Mexico");
            r3.setCuisine(countryMexico);
            r3.setCategory(categorySteakhouse);

            Restaurant r4 = new Restaurant();
            r4.setLocation(Location.of(-23.584589, -46.665405));
            r4.setName("Jardins Mexico");
            r4.setCuisine(countryMexico);
            r4.setCategory(categoryFit);

            Restaurant r5 = new Restaurant();
            r5.setLocation(Location.of(-23.557774, -46.635744));
            r5.setName("Jardins Sweeden");
            r5.setCuisine(countrySweden);
            r5.setCategory(categoryFit);

            restaurantRepository.saveAll(Arrays.asList(r1, r2, r3, r4, r5));

            // Question
            Question question = new Question();
            question.setQuestion("What kind of food do you like?");
            question.setMultiple(false);
            question.setOrdering(1);

            Answer answersFit = new Answer(question, "Fit", "Fit");
            Answer answersSteakhouse = new Answer(question, "Steakhouse", "Steakhouse");
            question.setAnswers(Arrays.asList(answersFit, answersSteakhouse));

            questionRepository.save(question);
            answerRepository.saveAll(Arrays.asList(answersFit, answersSteakhouse));

            Item item = new Item();
            item.setName("Grilled Steak");
            item.setRestaurant(r3);
            item.setCategories(Arrays.asList(new ItemCategory(item, categorySteakhouse)));

            Item item1 = new Item();
            item1.setName("Salad Combo");
            item1.setRestaurant(r5);
            item1.setCategories(Arrays.asList(new ItemCategory(item1, categoryFit), new ItemCategory(item1, categoryVeg)));

            Item item2 = new Item();
            item2.setName("Salad Combo 2");
            item2.setRestaurant(r5);
            item2.setCategories(Arrays.asList(new ItemCategory(item2, categoryFit), new ItemCategory(item2, categoryVeg)));

            Item item3 = new Item();
            item3.setName("Salad El Ranchito");
            item3.setRestaurant(r4);
            item3.setCategories(Arrays.asList(new ItemCategory(item3, categoryFit), new ItemCategory(item3, categoryVeg)));

            itemRepository.saveAll(Arrays.asList(item, item1, item2, item3));
        }
    }
}