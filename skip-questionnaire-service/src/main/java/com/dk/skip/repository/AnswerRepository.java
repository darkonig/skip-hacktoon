package com.dk.skip.repository;

import com.dk.skip.data.model.Answer;
import com.dk.skip.data.model.Question;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository  extends PagingAndSortingRepository<Answer, String> { }
