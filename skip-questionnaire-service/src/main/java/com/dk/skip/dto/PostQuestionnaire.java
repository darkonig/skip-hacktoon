package com.dk.skip.dto;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

public class PostQuestionnaire implements Serializable {

    @NotEmpty
    public List<PostQuestionDto> questions;

    public List<PostQuestionDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<PostQuestionDto> questions) {
        this.questions = questions;
    }

}
