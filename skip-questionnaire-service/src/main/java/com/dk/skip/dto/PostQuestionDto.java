package com.dk.skip.dto;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

public class PostQuestionDto implements Serializable {

    @NotEmpty
    private String questionId;

    @NotEmpty
    private List<PostAnswerDto> answers;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<PostAnswerDto> getAnswers() {
        return answers;
    }

    public void setAnswers(List<PostAnswerDto> answers) {
        this.answers = answers;
    }
}
