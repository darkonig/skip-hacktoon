package com.dk.skip.dto;

import com.dk.skip.data.model.Answer;

import javax.persistence.Column;
import java.io.Serializable;

public class GetAnswerDto implements Serializable {
    private String answer;

    private String value;

    public GetAnswerDto(Answer answer) {
        this.setAnswer(answer.getAnswer());
        this.setValue(answer.getValue());
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
