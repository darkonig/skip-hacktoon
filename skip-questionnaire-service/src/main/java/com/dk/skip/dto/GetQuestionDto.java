package com.dk.skip.dto;

import com.dk.skip.data.model.Answer;
import com.dk.skip.data.model.Question;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@ApiModel(value = "Question")
public class GetQuestionDto implements Serializable {
    @ApiModelProperty(notes = "Question")
    private String question;

    @ApiModelProperty(notes = "Informs if is a multiple question")
    private boolean multiple;

    @ApiModelProperty(notes = "Question show order")
    private int ordering;

    private List<GetAnswerDto> answers;

    public GetQuestionDto(Question question) {
        this.setQuestion(question.getQuestion());
        this.setMultiple(question.isMultiple());
        this.setOrdering(question.getOrdering());
        this.setAnswers(question.getAnswers().stream().map(e -> new GetAnswerDto(e))
                .collect(Collectors.toList()));
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    public List<GetAnswerDto> getAnswers() {
        return answers;
    }

    public void setAnswers(List<GetAnswerDto> answers) {
        this.answers = answers;
    }
}
