package com.dk.skip.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Questionnaire implements Serializable {

    public List<GetQuestionDto> questions = new ArrayList<>();

    public Questionnaire() {
    }

    public Questionnaire(List<GetQuestionDto> questionnaire) {
        this.setQuestions(questions);
    }

    public List<GetQuestionDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<GetQuestionDto> questions) {
        this.questions = questions;
    }
}
