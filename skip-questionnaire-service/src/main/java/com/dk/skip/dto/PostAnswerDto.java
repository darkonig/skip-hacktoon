package com.dk.skip.dto;

import javax.validation.constraints.NotEmpty;

public class PostAnswerDto {

    @NotEmpty
    private String answerId;

    @NotEmpty
    private String value;

    public PostAnswerDto() {
    }

    public PostAnswerDto(String answerId, String value) {
        this.answerId = answerId;
        this.value = value;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
