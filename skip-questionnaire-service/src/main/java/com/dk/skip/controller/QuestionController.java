package com.dk.skip.controller;

import com.dk.skip.data.model.Item;
import com.dk.skip.data.model.Restaurant;
import com.dk.skip.data.model.Question;
import com.dk.skip.dto.GetQuestionDto;
import com.dk.skip.dto.PostQuestionnaire;
import com.dk.skip.dto.Questionnaire;
import com.dk.skip.service.QuestionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/questionnaire")
public class QuestionController {

    @Inject
    private QuestionService service;

    @GetMapping
    @ApiOperation(value = "Gets the questionnaire",
            response = GetQuestionDto[].class,
            responseContainer = "List")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 204, message = "No questions")
    })
    public ResponseEntity<Questionnaire> getQuestionnaire() {
        Iterable<Question> allQuestions = service.getAllQuestions();

        List<GetQuestionDto> questions = new ArrayList<>();
        for (Question question : allQuestions) {
            questions.add(new GetQuestionDto(question));
        }

        if (questions.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        Questionnaire q = new Questionnaire();
        q.setQuestions(questions);

        return ResponseEntity.ok(q);
    }

    @PostMapping("restaurants")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Gets the restaurants based upon the questionnaire answers",
            response = Restaurant[].class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 204, message = "No restaurants related to the answers")
    })
    public ResponseEntity<List<Restaurant>> postQuestionnaireRestaurants(@RequestBody @Valid PostQuestionnaire questionnaire) {
        List<Restaurant> restaurants = service.getRestaurants(questionnaire);
        if (restaurants.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(restaurants);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Gets the foods based upon the questionnaire answers",
            response = Item[].class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 204, message = "No food related to the answers")
    })
    public ResponseEntity<List<Item>> postQuestionnaire(@RequestBody @Valid PostQuestionnaire questionnaire) {
        List<Item> items = service.getItems(questionnaire);
        if (items.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(items);
    }
}
