package com.dk.skip.service;

import com.dk.skip.data.model.Item;
import com.dk.skip.data.model.Restaurant;
import com.dk.skip.data.model.Question;
import com.dk.skip.dto.PostQuestionnaire;

import java.util.List;

public interface QuestionService {

    Iterable<Question> getAllQuestions();

    List<Restaurant> getRestaurants(PostQuestionnaire questionnaire);

    List<Item> getItems(PostQuestionnaire questionnaire);

}
