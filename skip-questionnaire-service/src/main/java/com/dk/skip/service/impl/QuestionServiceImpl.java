package com.dk.skip.service.impl;

import com.dk.skip.data.model.*;
import com.dk.skip.dto.PostQuestionnaire;
import com.dk.skip.repository.ItemRepository;
import com.dk.skip.repository.QuestionRepository;
import com.dk.skip.repository.RestaurantRepository;
import com.dk.skip.service.QuestionService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private QuestionRepository repository;
    private RestaurantRepository restaurantRepository;
    private ItemRepository itemRepository;

    @Inject
    public QuestionServiceImpl(QuestionRepository repository, RestaurantRepository restaurantRepository,
            ItemRepository itemRepository) {
        this.repository = repository;
        this.restaurantRepository = restaurantRepository;
        this.itemRepository = itemRepository;
    }

    public QuestionServiceImpl(QuestionRepository repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<Question> getAllQuestions() {
        return repository.findAll();
    }

    /**
     * Gets restaurants based on the user answers (questionnaire)
     *
     * @param questionnaire
     * @return
     */
    @Override
    public List<Restaurant> getRestaurants(PostQuestionnaire questionnaire) {
        List<Category> favCategories = getFavoriteCategories(questionnaire);

        return restaurantRepository.findByCategoryIn(favCategories);
    }

    @Override
    public List<Item> getItems(PostQuestionnaire questionnaire) {
        List<Category> favCategories = getFavoriteCategories(questionnaire);

        return itemRepository.findByCategoriesIn(favCategories);
    }

    public List<Category> getFavoriteCategories(PostQuestionnaire questionnaire) {
        Map<String, Long> answers = questionnaire.getQuestions().stream()
                .flatMap(e -> e.getAnswers().stream())
                .collect(Collectors.groupingBy(
                    e -> e.getValue(), Collectors.counting()
                ))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                .collect(LinkedHashMap::new, (m,e) -> {m.put(e.getKey(), e.getValue());}, Map::putAll);

        return answers.keySet().stream().limit(2) .map(e -> new Category(e))
                .collect(Collectors.toList());
    }

}
