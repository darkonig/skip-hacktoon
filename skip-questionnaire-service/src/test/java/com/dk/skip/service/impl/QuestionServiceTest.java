package com.dk.skip.service.impl;

import com.dk.skip.data.model.*;
import com.dk.skip.dto.PostAnswerDto;
import com.dk.skip.dto.PostQuestionnaire;
import com.dk.skip.dto.PostQuestionDto;
import com.dk.skip.repository.ItemRepository;
import com.dk.skip.repository.RestaurantCategoryCountryRepository;
import com.dk.skip.repository.RestaurantCategoryRepository;
import com.dk.skip.repository.RestaurantRepository;
import com.dk.skip.test.AbstractTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;


/**
 * @author Caio Andrade (dk)
 */

public class QuestionServiceTest extends AbstractTest {
    @Inject
    private QuestionServiceImpl service;

    @Inject
    private RestaurantRepository restaurantRepository;

    @Inject
    private RestaurantCategoryRepository restaurantCategoryRepository;

    @Inject
    private RestaurantCategoryCountryRepository restaurantCategoryCountryRepo;

    @Inject
    private ItemRepository itemRepository;

    private static boolean dbInit = false;

    @Before
    public void setup() {
        if (dbInit) {
            return;
        }
        dbInit = true;

        Category categoryFit = new Category("Fit");
        Category categoryVeg = new Category("Veg");
        Category categorySteakhouse = new Category("Steakhouse");

        restaurantCategoryRepository.saveAll(Arrays.asList(categoryFit, categoryVeg, categorySteakhouse));

        RestaurantCategoryCountry countryGermany = new RestaurantCategoryCountry("Germany");
        RestaurantCategoryCountry countryIceland = new RestaurantCategoryCountry("Iceland");
        RestaurantCategoryCountry countryMexico = new RestaurantCategoryCountry("Mexico");
        RestaurantCategoryCountry countrySweden = new RestaurantCategoryCountry("Sweden");

        restaurantCategoryCountryRepo.saveAll(Arrays.asList(countryGermany, countryIceland, countryMexico, countrySweden));

        Restaurant r1 = new Restaurant();
        r1.setLocation(Location.of(-23.559361, -46.634948));
        r1.setName("Jardins");
        r1.setCuisine(countryGermany);
        r1.setCategory(categoryVeg);

        Restaurant r2 = new Restaurant();
        r2.setLocation(Location.of(-23.559808, -46.635672));
        r2.setName("Jardins Iceland");
        r2.setCuisine(countryIceland);
        r2.setCategory(categoryFit);

        Restaurant r3 = new Restaurant();
        r3.setLocation(Location.of(-23.557774, -46.635744));
        r3.setName("Jardins Mexico");
        r3.setCuisine(countryMexico);
        r3.setCategory(categorySteakhouse);

        Restaurant r4 = new Restaurant();
        r4.setLocation(Location.of(-23.584589, -46.665405));
        r4.setName("Jardins Mexico");
        r4.setCuisine(countryMexico);
        r4.setCategory(categoryFit);

        Restaurant r5 = new Restaurant();
        r5.setLocation(Location.of(-23.557774, -46.635744));
        r5.setName("Jardins Sweeden");
        r5.setCuisine(countrySweden);
        r5.setCategory(categoryFit);

        restaurantRepository.saveAll(Arrays.asList(r1, r2, r3, r4, r5));

        Item item = new Item();
        item.setName("Grilled Steak");
        item.setRestaurant(r3);
        item.setCategories(Arrays.asList(new ItemCategory(item, categorySteakhouse)));

        Item item1 = new Item();
        item1.setName("Salad Combo");
        item1.setRestaurant(r5);
        item1.setCategories(Arrays.asList(new ItemCategory(item1, categoryFit), new ItemCategory(item1, categoryVeg)));

        Item item2 = new Item();
        item2.setName("Salad Combo 2");
        item2.setRestaurant(r5);
        item2.setCategories(Arrays.asList(new ItemCategory(item2, categoryFit), new ItemCategory(item2, categoryVeg)));

        Item item3 = new Item();
        item3.setName("Salad El Ranchito");
        item3.setRestaurant(r4);
        item3.setCategories(Arrays.asList(new ItemCategory(item3, categoryFit), new ItemCategory(item3, categoryVeg)));

        itemRepository.saveAll(Arrays.asList(item, item1, item2, item3));



        /*
        Question question = new Question();
        question.setQuestion("What do you prefere?");
        question.setOrdering(1);

        Answer answersFit = new Answer(question, "Fit", "Fit");
        Answer answersSteakhouse = new Answer(question, "Steakhouse", "Steakhouse");*/
    }

    @Test
    public void shouldGetRestaurants() {
        PostQuestionDto question1 = new PostQuestionDto();
        question1.setAnswers(Arrays.asList(new PostAnswerDto("", "Fit")));

        PostQuestionDto question2 = new PostQuestionDto();
        question2.setAnswers(Arrays.asList(new PostAnswerDto("", "Veg")));

        PostQuestionnaire post = new PostQuestionnaire();
        post.setQuestions(Arrays.asList(question1, question2));

        List<Restaurant> restaurants = service.getRestaurants(post);

        assertThat(restaurants, hasSize(4));
    }

    @Test
    public void shouldGetItems() {
        PostQuestionDto question1 = new PostQuestionDto();
        question1.setAnswers(Arrays.asList(new PostAnswerDto("", "Fit")));

        PostQuestionDto question2 = new PostQuestionDto();
        question2.setAnswers(Arrays.asList(new PostAnswerDto("", "Veg")));

        PostQuestionnaire post = new PostQuestionnaire();
        post.setQuestions(Arrays.asList(question1, question2));

        List<Item> items = service.getItems(post);

        assertThat(items, hasSize(3));
    }

    @Test
    public void shouldGetEmptyRestaurants() {
        PostQuestionDto question1 = new PostQuestionDto();
        question1.setAnswers(Arrays.asList(new PostAnswerDto("", "Bla1")));

        PostQuestionDto question2 = new PostQuestionDto();
        question2.setAnswers(Arrays.asList(new PostAnswerDto("", "Xyz")));

        PostQuestionnaire post = new PostQuestionnaire();
        post.setQuestions(Arrays.asList(question1, question2));

        List<Restaurant> restaurants = service.getRestaurants(post);

        assertThat(restaurants, hasSize(0));
    }

    @Test
    public void shouldGetEmptyItems() {
        PostQuestionDto question1 = new PostQuestionDto();
        question1.setAnswers(Arrays.asList(new PostAnswerDto("", "Bla")));

        PostQuestionDto question2 = new PostQuestionDto();
        question2.setAnswers(Arrays.asList(new PostAnswerDto("", "Xio")));

        PostQuestionnaire post = new PostQuestionnaire();
        post.setQuestions(Arrays.asList(question1, question2));

        List<Item> items = service.getItems(post);

        assertThat(items, hasSize(0));
    }
}
