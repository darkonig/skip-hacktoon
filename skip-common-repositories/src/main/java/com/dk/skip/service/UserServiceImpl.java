package com.dk.skip.service;

import com.dk.skip.data.model.User;
import com.dk.skip.exception.EmailExistsException;
import com.dk.skip.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Inject
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User registerNewUserAccount(User user) {
        if (getByEmail(user.getEmail()) != null) {
            throw new EmailExistsException("There is an account with that email adress:" + user.getEmail());
        }

        return userRepository.save(user);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}