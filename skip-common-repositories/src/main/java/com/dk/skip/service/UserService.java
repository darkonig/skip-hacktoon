package com.dk.skip.service;

import com.dk.skip.data.model.User;

public interface UserService {

    User registerNewUserAccount(User userDto);

    User getByEmail(String email);

}
