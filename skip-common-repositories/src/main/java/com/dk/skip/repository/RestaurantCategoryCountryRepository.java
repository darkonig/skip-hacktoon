package com.dk.skip.repository;

import com.dk.skip.data.model.RestaurantCategoryCountry;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Caio Andrade (dk)
 */
public interface RestaurantCategoryCountryRepository extends PagingAndSortingRepository<RestaurantCategoryCountry, String> {
}
