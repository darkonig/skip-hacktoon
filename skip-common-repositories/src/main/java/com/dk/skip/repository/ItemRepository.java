package com.dk.skip.repository;

import com.dk.skip.data.model.Category;
import com.dk.skip.data.model.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<Item, String> {

    @Query("select distinct i.item from ItemCategory i join i.item where i.category in :categories")
    List<Item> findByCategoriesIn(@Param("categories") List<Category> categories);

}
