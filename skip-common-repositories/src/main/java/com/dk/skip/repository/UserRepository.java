package com.dk.skip.repository;

import com.dk.skip.data.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    User findByEmail(String email);

    User findByEmailAndActive(String email, boolean active);

}