package com.dk.skip.repository;

import com.dk.skip.data.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantCategoryRepository  extends CrudRepository<Category, String> {
}
