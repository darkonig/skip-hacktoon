package com.dk.skip.repository;

import com.dk.skip.data.model.Location;
import com.dk.skip.data.model.Restaurant;
import com.dk.skip.data.model.Category;
import com.dk.skip.data.model.RestaurantCategoryCountry;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
@Repository
public interface RestaurantRepository extends PagingAndSortingRepository<Restaurant, String> {

    @Query("from Restaurant r " +
            "where " +
            "   r.cuisine in :category and " +
            "   r.location.latitude > :#{#l3.latitude} and" +
            "   r.location.latitude < :#{#l1.latitude} and " +
            "   r.location.longitude > :#{#l4.longitude} and" +
            "   r.location.longitude < :#{#l2.longitude}")
    List<Restaurant> findNearest(
            @Param("category") Iterable<RestaurantCategoryCountry> category,
            @Param("l1") Location l1,
            @Param("l2") Location l2,
            @Param("l3") Location l3,
            @Param("l4") Location l4);

    List<Restaurant> findByCategoryIn(List<Category> category);

    List<Restaurant> findByCategory(Category category, Pageable page);
}
