package com.dk.skip.repository;

import com.dk.skip.data.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Caio Andrade (dk)
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, String> {

}
