package com.dk.skip.exception;

import org.springframework.dao.DataIntegrityViolationException;

public class EmailExistsException extends DataIntegrityViolationException {

	public EmailExistsException(String message) {
		super(message);
	}

}
