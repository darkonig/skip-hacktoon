package com.dk.skip.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FixtureListResource {

    private List<FixtureResource> fixtures;

    public List<FixtureResource> getFixtures() {
        return fixtures;
    }

    public void setFixtures(List<FixtureResource> fixtures) {
        this.fixtures = fixtures;
    }
}
