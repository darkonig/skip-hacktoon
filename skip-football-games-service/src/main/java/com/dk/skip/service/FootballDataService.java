package com.dk.skip.service;

import com.dk.skip.model.FixtureListResource;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Caio Andrade (dk)
 */
@FeignClient(name = "footballDataService", url = "http://api.football-data.org/v1/competitions/")
public interface FootballDataService {

    @RequestMapping(value = "{competition}/fixtures", method = RequestMethod.GET)
    FixtureListResource getFixtures(@PathVariable("competition") String league);

}
