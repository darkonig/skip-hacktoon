package com.dk.skip.service;

import com.dk.skip.data.model.Location;
import com.dk.skip.data.model.Restaurant;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
public interface MatchesService {

    public List<Restaurant> getPromotionalRestaurants(Location location, LocalDate date);

}
