package com.dk.skip.service.impl;

import com.dk.skip.core.LocationUtil;
import com.dk.skip.data.model.Location;
import com.dk.skip.data.model.Restaurant;
import com.dk.skip.data.model.RestaurantCategoryCountry;
import com.dk.skip.model.FixtureListResource;
import com.dk.skip.repository.RestaurantCategoryCountryRepository;
import com.dk.skip.repository.RestaurantRepository;
import com.dk.skip.service.FootballDataService;
import com.dk.skip.service.MatchesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Caio Andrade (dk)
 */
@Service
public class MatchesServiceImpl implements MatchesService {

    private FootballDataService footballDataService;
    private RestaurantRepository repository;
    private RestaurantCategoryCountryRepository categoryCountryRepository;

    @Value("${football.competition.id}")
    private String league;

    @Inject
    public MatchesServiceImpl(FootballDataService footballDataService, RestaurantRepository repository, RestaurantCategoryCountryRepository categoryCountryRepository) {
        this.footballDataService = footballDataService;
        this.repository = repository;
        this.categoryCountryRepository = categoryCountryRepository;
    }

    @Override
    public List<Restaurant> getPromotionalRestaurants(Location location, LocalDate date) {
        FixtureListResource fixtures = footballDataService.getFixtures(league);

        List<String> fixtureTeams = fixtures.getFixtures().stream().filter(e -> e.getDate().toLocalDate().equals(date))
                .flatMap(e -> Arrays.asList(e.getAwayTeamName()
                                        , e.getHomeTeamName()).stream())
                .collect(Collectors.toList());

        Iterable<RestaurantCategoryCountry> teams = categoryCountryRepository.findAllById(fixtureTeams);

        //get the reach range
        final double radius = 2000; //5 km
        final double mult = 1.1;

        Location l0 = LocationUtil.calculateDerivedPosition(location, mult * radius, 0);
        Location l90 = LocationUtil.calculateDerivedPosition(location, mult * radius, 90);
        Location l180 = LocationUtil.calculateDerivedPosition(location, mult * radius, 180);
        Location l270 = LocationUtil.calculateDerivedPosition(location, mult * radius, 270);

        // get nearest restaurants
        return repository.findNearest(teams, l0, l90, l180, l270);
    }

}
