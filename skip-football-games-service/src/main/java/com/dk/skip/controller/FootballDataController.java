package com.dk.skip.controller;

import com.dk.skip.data.model.Location;
import com.dk.skip.data.model.Restaurant;
import com.dk.skip.dto.GetFootballMatch;
import com.dk.skip.service.MatchesService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
@RestController
@RequestMapping("api/v1/matches")
public class FootballDataController {

    @Inject
    private MatchesService service;

    @GetMapping("restaurants")
    @ApiOperation(value = "Get the nearest restaurants to the given location, based upon the day of the given " +
            "world cup football matche (2018)",
            response = Restaurant[].class,
            responseContainer = "List")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 204, message = "No available restaurants base on the filter")
    })
    public ResponseEntity<List<Restaurant>> getMatchingRestaurants(GetFootballMatch param) {
        LocalDate date;
        if (param.getDate() == null) {
            date = LocalDate.now();
        } else {
            date = LocalDate.parse(param.getDate());
        }

        List<Restaurant> restaurants = service.getPromotionalRestaurants(
                Location.of(param.getLatitude(), param.getLongitude()), date);
        if (restaurants == null || restaurants.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(restaurants);
    }

}
