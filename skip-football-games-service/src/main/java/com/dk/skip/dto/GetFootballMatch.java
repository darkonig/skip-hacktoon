package com.dk.skip.dto;

import com.dk.skip.data.model.Location;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;

@ApiModel(value = "FootballMatch")
public class GetFootballMatch {
    @NotEmpty
    @ApiModelProperty(value = "longitude", notes = "Request location", example = "-23.560005")
    private Double longitude;

    @NotEmpty
    @ApiModelProperty(value = "latitude", notes = "Request location", example = "-46.635152")
    private Double latitude;

    @ApiModelProperty(value = "date", notes = "Match date (ex.: 2018-06-23)", example = "2018-06-23")
    private String date;

    public GetFootballMatch() {
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
