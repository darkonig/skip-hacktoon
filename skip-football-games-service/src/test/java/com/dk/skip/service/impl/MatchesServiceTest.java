package com.dk.skip.service.impl;

import com.dk.skip.data.model.Location;
import com.dk.skip.data.model.Restaurant;
import com.dk.skip.data.model.RestaurantCategoryCountry;
import com.dk.skip.repository.RestaurantCategoryCountryRepository;
import com.dk.skip.repository.RestaurantRepository;
import com.dk.skip.service.FootballDataService;
import com.dk.skip.service.MatchesService;
import com.dk.skip.test.AbstractTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;


/**
 * @author Caio Andrade (dk)
 */

@EnableFeignClients(clients = FootballDataService.class)
public class MatchesServiceTest extends AbstractTest {

    @Inject
    private MatchesService service;

    @Inject
    private RestaurantRepository restaurantRepository;

    @Inject
    private RestaurantCategoryCountryRepository restaurantCategoryCountryRepo;

    @Before
    public void setup() {
        RestaurantCategoryCountry countryGermany = new RestaurantCategoryCountry("Germany");
        RestaurantCategoryCountry countryIceland = new RestaurantCategoryCountry("Iceland");
        RestaurantCategoryCountry countryMexico = new RestaurantCategoryCountry("Mexico");
        RestaurantCategoryCountry countrySweden = new RestaurantCategoryCountry("Sweden");

        restaurantCategoryCountryRepo.saveAll(Arrays.asList(countryGermany, countryIceland, countryMexico, countrySweden));

        Restaurant r1 = new Restaurant();
        r1.setLocation(Location.of(-23.559361, -46.634948));
        r1.setName("Jardins");
        r1.setCuisine(countryGermany);

        Restaurant r2 = new Restaurant();
        r2.setLocation(Location.of(-23.559808, -46.635672));
        r2.setName("Jardins Iceland");
        r2.setCuisine(countryIceland);

        Restaurant r3 = new Restaurant();
        r3.setLocation(Location.of(-23.557774, -46.635744));
        r3.setName("Jardins Mexico");
        r3.setCuisine(countryMexico);

        Restaurant r4 = new Restaurant();
        r4.setLocation(Location.of(-23.584589, -46.665405));
        r4.setName("Jardins Mexico");
        r4.setCuisine(countryMexico);

        Restaurant r5 = new Restaurant();
        r5.setLocation(Location.of(-23.557774, -46.635744));
        r5.setName("Jardins Sweeden");
        r5.setCuisine(countrySweden);

        restaurantRepository.saveAll(Arrays.asList(r1, r2, r3, r4, r5));
    }

    @Test
    public void shouldGetPromotionalRestaurants() {
        List<Restaurant> promotionalRestaurants = service.getPromotionalRestaurants(Location.of(-23.560005, -46.635152)
                , LocalDate.parse("2018-06-23"));

        assertThat(promotionalRestaurants, hasSize(3));
    }
}
