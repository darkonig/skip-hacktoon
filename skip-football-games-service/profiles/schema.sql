create schema if not exists skip;

/* drop table skip.items if exists;
drop table skip.orders if exists;
drop table skip.restaurants if exists;
drop table skip.roles if exists;
drop table skip.users if exists;
drop table skip.users_roles if exists;
create table skip.items (id varchar(32) not null, name varchar(30) not null, value float not null, restaurant_id varchar(32) not null, primary key (id));
create table skip.orders (id varchar(32) not null, delivery_fee float not null, expected_collection timestamp, scheduled timestamp, status integer, value float not null, restaurant_id varchar(32), user_id varchar(32), primary key (id));
create table skip.restaurants (id varchar(32) not null, latitude double, longitude double, name varchar(30) not null, primary key (id));
create table skip.roles (role varchar(50) not null, primary key (role));
create table skip.users (id varchar(32) not null, active boolean not null, email varchar(100) not null, first_name varchar(30) not null, last_name varchar(30) not null, password varchar(60) not null, primary key (id));
create table skip.users_roles (user_id varchar(32) not null, roles_role varchar(50) not null);
alter table skip.users add constraint uk_users_email unique (email);
alter table skip.users_roles add constraint users_roles unique (roles_role);
alter table skip.items add constraint fk_items_restaurants foreign key (restaurant_id) references restaurants;
alter table skip.orders add constraint fk_orders_restaurants foreign key (restaurant_id) references restaurants;
alter table skip.orders add constraint fk_orders_users foreign key (user_id) references users;
alter table skip.users_roles add constraint FK3k34bms5ip9m02vrm2ws0srlf foreign key (roles_role) references roles;
alter table skip.users_roles add constraint FK2o0jvgh89lemvvo17cbqvdxaa foreign key (user_id) references users;*/
