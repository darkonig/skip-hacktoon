```mermaid
sequenceDiagram
    participant Client
    participant Restaurant
    Client->>Restaurant: Schedules an order
    loop Healthcheck
        Restaurant->>Restaurant: Can handle?
    end
    Note right of Restaurant: Rational thoughts <br/>prevail...
    Restaurant-->>Client: Denied
    Restaurant->>Client: Ok
    loop thru restaurants
        Restaurant->>Courier: Gets the order
    end
    Courier->>Client: Delivery
```

```sequence
Client->Order: Places
Restaurant-->Order: Reads
Restaurant->>Courier: Send
Restaurant->>Courier: Send
Restaurant->>Courier: Send
Courier->>Client: Delivery
```

---

```sequence
Client->App: Access
App-->FootbalService: Request Cup Matches
FootbalService->>App: Cup Macthes
App->>Client: Nearest restaurants*
Note left of App: * Based on\nthe returned\nmatched of\nthe day
```
