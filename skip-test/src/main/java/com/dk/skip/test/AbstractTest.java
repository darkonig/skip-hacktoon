package com.dk.skip.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Caio Andrade (dk)
 */
/*@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationConfiguration.class)
@Sql({"/src/test/resources/insert-data.sql"})
@Sql(scripts = "/clean-up.sql", executionPhase = AFTER_TEST_METHOD)*/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationConfiguration.class)
@ActiveProfiles("test")
public abstract class AbstractTest {

}