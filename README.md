# Skip Project

## About the project

This application is composed by 6 projects:
**skip-entities** - Database Entities
**skip-common-repositories** - Common repositories, which will be used by the entire application
**skip-football-games-service** - Service who gets restaurants available on a specific range from a given place.
**skip-questionnaire-service** - Show a questionnaire to the user answer, and base upon his answer the app is able to return
                                 some restaurants or some food suggestions.
**skip-api** - Application executor
**skip-test** - Common test classes

    Obs.: the skip-football-games-service and skip-questionnaire-service were intended to be separated microservices,
    but due to the time wasn't possible to create as intended.

## Building

```cmd
mvnw spring:boot run
```

## Database

The database used to this application is the H2, but we could encounter a higher performance
with was design on a NoSQL (like MongoDB).

## Documentation

The documentation can be founded running the application, and accessing the following url:
`http://localhost:8080/swagger-ui.html`.

There is possible to find all the available info to execute the application.

## Service Request

Get the nearest restaurants to the given location, based upon the day of the given world cup football matche (2018).
    obs.: Only a few restaurants registered.

@GET: `/api/v1/matches/restaurants`
Params:
    - lantidude: -23.560005
    - longitude: -46.635152
    - date: 2018-06-23


Gets the questionnaire
    obs.: Only 1 registered.
@GET: `/api/v1/questionnaire`

Gets the foods based upon the questionnaire answers
@POST: `/api/v1/questionnaire/restaurants`
Param: {
         "questions": [
           {
             "answers": [
               {
                 "answerId": "",
                 "value": "Fit"
               }
             ],
             "questionId": ""
           }
         ]
       }

Gets the restaurants based upon the questionnaire answers
@POST: `/api/v1/questionnaire`
Param: {
         "questions": [
           {
             "answers": [
               {
                 "answerId": "",
                 "value": "Fit"
               }
             ],
             "questionId": ""
           }
         ]
       }

## Used Technologies

- Java 8
- Spring boot
- Swagger
- H2
- JUnit/Hamcrest/Mockito
- Spring Web MVC
- Spring Cloud Feign